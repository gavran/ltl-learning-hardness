import argparse
import pdb

from Word import Word
from Pattern import Pattern

empty_symbol = "E"
all_symbol = "A"

def generate_sample(n):
    positive = Word([all_symbol for _ in range(n)] + [all_symbol for _ in range(2*n)] + [empty_symbol, all_symbol], lassoStart=3*n)
    negative = []
    for r in range(n):
        neg_trace = [all_symbol for _ in range(r)] + [empty_symbol] + [all_symbol for _ in range(n-r-1)] +  \
                    [all_symbol for _ in range(2*n)] + [empty_symbol, all_symbol]
        neg_word = Word(neg_trace, lassoStart=3*n)
        negative.append(neg_word)

    return positive, negative

def generate_modified_sample(n):

    finite_pos_part = [all_symbol for _ in range(n)]# + [all_symbol for _ in range(2 * n)]

    finite_base_lists = [[all_symbol for _ in range(r)] + [empty_symbol] + [all_symbol for _ in range(n - r - 1)]
                for r in range(n)]

    finite_base = finite_pos_part + [el for sublist in finite_base_lists for el in sublist]
    lasso = finite_base


    positive = Word(finite_pos_part + lasso, lassoStart=len(finite_pos_part))
    negative = []
    for r in range(n):
        finite_part = [all_symbol for _ in range(r)] + [empty_symbol] + [all_symbol for _ in range(n - r - 1)]
        #lasso is the same for as in the positive word
        neg_trace =  finite_part + lasso
        neg_word = Word(neg_trace, lassoStart=len(finite_part))
        negative.append(neg_word)

    return positive, negative


def get_pattern(num_U, num_X, alpha):
    p = Pattern()
    return p.get_pat(num_U, num_X, alpha)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--sample_word_length", type=int, default=5)
    args = parser.parse_args()
    positive, negative = generate_modified_sample(args.sample_word_length)
    print(positive, negative)
    sample_size = len(negative) + 1




    for num_U in range(args.sample_word_length):
        for num_X in range(args.sample_word_length):
            if num_X + num_U >= 2*args.sample_word_length or (num_X >= args.sample_word_length - 1 and num_U > 0):
                continue
            dict_of_patterns = {}
            neg_patterns = []
            positive_pattern = get_pattern(num_U, num_X, positive).prettyPrint()
            dict_of_patterns[positive] = positive_pattern
            overlap = False
            for trace in negative:

                pattern = get_pattern(num_U, num_X, trace).prettyPrint()
                dict_of_patterns[trace] = pattern
                if pattern == positive_pattern:
                    print("overlap: {} for trace {} (for U {} and X {})".format(pattern, trace, num_U, num_X))
                    overlap = True
                    break
            if overlap is False:
                print("\n\n")
                print("============== num_U: {}, num_X: {} ===============".format(num_U, num_X))
                for dict_key in sorted(dict_of_patterns.keys()):
                    print("{}: {}".format(dict_key, dict_of_patterns[dict_key]))



if __name__ == '__main__':
    main()