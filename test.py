import pdb

from Pattern import Pattern
from Word import Word

def main():
    w = Word("abbbacbacba", 9)
    #w = Word("abb",1)
    print("w = {}".format(w))

    p = Pattern()
    #pdb.set_trace()
    d1 = p.get_pat(0,0,w)
    print("pat(0, 0, w) = {}".format(d1))


    d2 = p.get_patword(0,0, w)
    print("patword(0,0,w) = {}".format(d2))



    d4 = p.get_pat(1, 0, w)
    print("pat(1,0,w) = {}".format(d4))



    d3 = p.get_patword(1,0,w)
    print("patword(1,0,w) = {}".format(d3))



    d5 = p.get_pat(2,0,w)
    print("pat(2,0,w) = {}".format(d5))

    d6 = p.get_pat(0,1,w)
    print("pat(0,1,w) = {}".format(d6))

    d7 = p.get_patword(0, 1, w)
    print("patword(0,1,w) = {}".format(d7))
    #print("pretty patword(0,1,w) = {}".format(d7.prettyPrint()))

    d8 = p.get_pat(1,1,w)
    print("pat(1,1,w) = {}".format(d8))
    #print("pretty pat(1,1,w) = {}".format(d8.prettyPrint()))





if __name__ == '__main__':
    main()