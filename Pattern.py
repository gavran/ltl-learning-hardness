import pdb
from Word import Word


def _remove_repetitions(alpha):

    list_without_repetitions = []
    word_with_repeated_lasso = alpha.word + alpha.word[alpha.lassoStart :]
    for symb in word_with_repeated_lasso:
        if not symb in list_without_repetitions:

            list_without_repetitions.append(symb)

    return list_without_repetitions


class Pattern:
    def __init__(self):

        # initialize pat
        self.pat = {}

        # initialize patword
        self.patword = {}

    def get_patword(self, m, n, alpha):


        if (m,n,alpha) in self.patword:
            return self.patword[(m,n,alpha)]

        patword_list = []
        for i in range(alpha.representation_length):
            #print("i = {}".format(i))

            new_word = alpha[i:]
            #print("new word = {}".format(new_word))

            patword_list.append(self.get_pat(m,n,new_word))

        self.patword[(m,n,alpha)] = Word(patword_list, alpha.lassoStart)
        return self.patword[(m,n,alpha)]


    """
     always returns a finite word
    """
    def get_pat(self, m, n, alpha):

        if m < 0 or n < 0:
            raise ValueError("m and n should be >= 0")


        if (m, n, alpha) in self.pat:
            return self.pat[(m,n,alpha)]

        if m == 0:
            self.pat[(m,n,alpha)] = alpha[:(n+1)]

        elif m > 0:
            #pdb.set_trace()
            patword = self.get_patword(m-1, n, alpha)

            self.pat[(m,n,alpha)] = Word(_remove_repetitions(patword))

        return self.pat[(m,n,alpha)]


