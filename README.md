# LTL Patterns
This is a quick implementation of the concept of characteristic patterns for LTL from the paper 
[Kucera & Strejcek: Characteristic Patterns for LTL](https://link.springer.com/chapter/10.1007%2F978-3-540-30577-4_27)


## Running
 - make sure to run with python3
 - *pat*s and *patword*s from Example 1 from the paper can be obtained by running `python3 test.py` 
 (this functions as a sanity check) 