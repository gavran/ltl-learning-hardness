import pdb
import sys

if not sys.version_info.major == 3:
    raise EnvironmentError("make sure to run with python3")

INF = float('inf')

class Word:
    def __init__(self, word, lassoStart=None):

        self.word = list(word)

        self.lassoStart = lassoStart
        self.representation_length = len(self.word)

        if self.lassoStart is None:
            self.initPart = self.word
            self.wordLength = self.representation_length
            self.finitePartLength = self.wordLength
            self.loopLength = None
        else:
            if self.lassoStart >= len(self.word):
                raise ValueError("lasso start must be inside the word")
            self.wordLength = INF
            self.finitePartLength = self.lassoStart
            self.initPart = self.word[: self.finitePartLength]
            self.loopLength = len(self.word) - self.finitePartLength


    def _next(self, i):
        if i == self.wordLength-1:
            raise ValueError("now next, at the end of the word")
        if i < self.representation_length - 1:
            return i+1
        if i == self.representation_length - 1:
            return self.lassoStart


    def __len__(self):
        return self.representation_length

    def __hash__(self):
        return hash(self.__repr__())

    def __lt__(self, other):
        return self.__repr__() <= other.__repr__()

    def __getitem__(self, item):

        if isinstance(item, slice):

            ret_seq = []
            if item.start is None:
                start = 0
            else:
                start = item.start

            if start < 0:
                raise ValueError("value for start of the slice must be at least 0")


            # the [x:] notation gives slice(i, None, None)
            if item.stop is None:

                if self.lassoStart is None:
                    return Word(self.word[start :], None)

                elif start > self.lassoStart:
                    finite_part = self.word[start :]
                    lasso_part = self.word[self.lassoStart :]
                    return Word(finite_part + lasso_part, len(finite_part))
                else:
                    finite_part = self.word[start:self.lassoStart]
                    lasso_part = self.word[self.lassoStart :]
                    return Word(finite_part + lasso_part, len(finite_part))

            # looking for a finite word
            else:
                if item.stop > self.wordLength:
                    raise ValueError("word is shorter thant the requested slice")

                for i in range(start, item.stop):
                    ret_seq.append(self.__getitem__(i))

                return Word(ret_seq, None)


        # looking for only one element
        else:

            if item > self.wordLength:
                raise ValueError("word is shorter thant the requested element")
            if item < len(self.word):
                return self.word[item]
            else:
                return self.word[self.lassoStart + (item - self.lassoStart) % self.loopLength]


    def __eq__(self, other):
        return self.__repr__() == other.__repr__()

    # replace every symbol of a word by another one
    def replace(self, replacement_dict):
        if not set([str(w) for w in self.word]).issubset(set(replacement_dict.keys())):
            print(set(self.word))
            print(set(replacement_dict.keys()))
            raise ValueError("replacement not complete")
        new_word = []
        for w in self.word:
            new_word.append(replacement_dict[str(w)])

        return Word(new_word, lassoStart=self.lassoStart)

    def prettyPrint(self):

        if self.lassoStart is None:
            finite_part = self.word[ 0 : self.finitePartLength]
            s = ",".join([str(symb) for symb in finite_part])

        else:
            s = ",".join([str(symb) for symb in self.word[: self.lassoStart]]) + ",(" + ",".join(
                [str(symb) for symb in self.word[self.lassoStart :]]) + ")*"
        return s
    """
    still not happy with the representation, should be better
    """
    def __repr__(self):
        finite_part_list = self.word[: self.finitePartLength]

        if self.lassoStart is None:
            s = "".join([str(symb) for symb in finite_part_list])
        else:
            lasso_part_list = self.word[self.lassoStart:]

            s = "".join([str(symb) for symb in finite_part_list]) + "(" + "".join([str(symb) for symb in lasso_part_list]) + ")*"
        return s


